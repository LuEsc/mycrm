from dataclasses import fields
from rest_framework import serializers
from .models import TypePerson

class TypePersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypePerson
        fields = [
            'name',
            'description',
        ]
    
    