from django.forms import model_to_dict
from django.shortcuts import render
from django.http import JsonResponse

from rest_framework.decorators import api_view
from rest_framework.response import Response

from api.models import TypePerson
from api.serializers import TypePersonSerializer

# Create your views here.
@api_view(['GET'])
def vie_home(request):
    instances = TypePerson.objects.all()
    serializer = TypePersonSerializer(instances, many=True)
    return Response(serializer.data)


# Metodo para crear un objeto hacia mi modelo
@api_view(['POST'])
def create_type_person(request):
    serializer = TypePersonSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    else:
        return Response(serializer.errors)

@api_view(['GET', 'PUT', 'DELETE'])
def typePush(request, pk):
    instances = TypePerson.objects.get(pk=pk)
    if request.method == 'GET':
        serializer = TypePersonSerializer(instances)
        return Response(serializer.data)

    if request.method == 'PUT':
        serializer = TypePersonSerializer(instances, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return serializer.errors

    if request.method == 'DELETE':
        instances.delete()
        return Response({
            'delete': True
        })


