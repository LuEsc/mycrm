from django.db import models
from django.forms import IntegerField

# Create your models here.

class TypePerson(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name

class Person(models.Model):
    nit = models.BigIntegerField()
    name = models.CharField(max_length=255, null=True, blank=True)
    lastname = models.CharField(max_length=255, null=True, blank=True)
    email = models.CharField(max_length=255, null=True, blank=True)
    phone = models.CharField(max_length=255, null=True, blank=True)
    age = models,IntegerField()
    address = models.TextField(null=True, blank=True)
    birthday = models.DateTimeField(null=True, blank=True)
    type_person = models.ForeignKey(TypePerson, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        abstract = True