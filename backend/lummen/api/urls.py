from django.urls import path
from . import views


# Creamos el scafold de urls de la app API
urlpatterns = [
    path('list/', views.vie_home), # Esta ruta hace referencia a: localhost:8000/
    path('add/', views.create_type_person),
    path('list/<int:pk>', views.typePush)
]
