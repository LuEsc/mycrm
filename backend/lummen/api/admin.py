from django.contrib import admin

from api.models import TypePerson

# Register your models here.
admin.site.register(TypePerson)