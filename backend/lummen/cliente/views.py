from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from cliente.serializers import PersonSerializer

from cliente.models import Client
# Create your views here.

@api_view(['GET'])
def list_person(request):
    instances = Client.objects.all()
    serializer = PersonSerializer(instances, many=True)
    return Response(serializer.data)


# Metodo para crear un objeto hacia mi modelo
@api_view(['POST'])
def create_person(request):
    serializer = PersonSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    else:
        return Response(serializer.errors)

@api_view(['GET', 'PUT', 'DELETE'])
def personPush(request, pk):
    instances = Client.objects.get(pk=pk)
    if request.method == 'GET':
        serializer = PersonSerializer(instances)
        return Response(serializer.data)

    if request.method == 'PUT':
        serializer = PersonSerializer(instances, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return serializer.errors

    if request.method == 'DELETE':
        instances.delete()
        return Response({
            'delete': True
        })