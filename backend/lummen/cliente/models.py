from pyexpat import model
from django.db import models

from api.models import Person

# Create your models here.
class Client(Person):
    producto_buy = models.CharField(max_length=255, null=True, blank=True)
    credit_days = models.IntegerField()

    def __str__(self):
        return self.name