from django.urls import path
from . import views


# Creamos el scafold de urls de la app API
urlpatterns = [
    path('list/', views.list_person), # Esta ruta hace referencia a: localhost:8000/api/person
    path('add/', views.create_person),
    path('list/<int:pk>', views.personPush)
]
