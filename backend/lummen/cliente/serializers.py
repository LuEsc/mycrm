from dataclasses import fields
from rest_framework import serializers
from .models import Client

class PersonSerializer(serializers.ModelSerializer):
    type_person = serializers.StringRelatedField()
    class Meta:
        model = Client
        fields = '__all__'