import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConsumerapiService {
readonly APIurl = "http://127.0.0.1:8000/"; //URL DE MI API DESARROLLADA EN DJANGO


  constructor(private http:HttpClient) { }

  //Metodo para obtener de mi API la lista de tipo de persona
  getListTypeP():Observable<any[]>{
    return this.http.get<any[]>(this.APIurl + 'api/list/');
  }

  //Metodo para agregar a mi API un tipo de persona
  addTypeP(val:any){
    return this.http.post(this.APIurl + 'api/add/', val);
  }

  //Metodo para actualizar de mi API un tipo de persona
  updateTypeP(val:any){
    return this.http.put(this.APIurl + 'api/list/', val);
  }

  //Metodo para eliminar de mi API un tipo de persona
  deleteTypeP(val:any){
    return this.http.delete(this.APIurl + 'api/list/' + val);
  }

  /***************************Endpoints para consumir las operaciones de cliente****************************** */
  
  //Metodo para obtener de mi API la lista de tipo de persona
  getListPerson():Observable<any[]>{
    return this.http.get<any[]>(this.APIurl + 'api/person/list/');
  }

  //Metodo para agregar a mi API un tipo de persona
  addPerson(val:any){
    return this.http.post(this.APIurl + 'api/person/add/', val);
  }

  //Metodo para actualizar de mi API un tipo de persona
  updatePerson(val:any, id:number){
    return this.http.put<any>(this.APIurl + 'api/person/list/' + id, val);
  }

  //Metodo para eliminar de mi API un tipo de persona
  deletePerson(id:number){
    return this.http.delete<any>(this.APIurl + 'api/person/list/' + id);
  }

}

