import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddClientComponent } from './client/add-client/add-client.component';
import { ClientComponent } from './client/client.component';
import { TypeclientComponent } from './typeclient/typeclient.component';

const routes: Routes = [
  {path: 'typerson', component:TypeclientComponent},
  {path: 'client', component:ClientComponent},
  {path: 'addPerson', component:AddClientComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }