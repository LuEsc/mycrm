import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ConsumerapiService } from '../consumerapi.service';

@Component({
  selector: 'app-typeclient',
  templateUrl: './typeclient.component.html',
  styleUrls: ['./typeclient.component.css']
})
export class TypeclientComponent implements OnInit {


  constructor(private _consumerservice:ConsumerapiService) { }
  typersons:any=[];
  displayedColumns: string[] = ['name', 'description', 'action'];
  dataSource: MatTableDataSource<any>=new MatTableDataSource<any>();


  ngOnInit(): void {
    this._consumerservice.getListTypeP().subscribe(
      (typePersonData)=>{
        this.typersons=typePersonData
        this.dataSource = new MatTableDataSource<any>(this.typersons);
      }
    )
  }

}
