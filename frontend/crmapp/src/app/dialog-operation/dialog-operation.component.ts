import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConsumerapiService } from '../consumerapi.service';

@Component({
  selector: 'app-dialog-operation',
  templateUrl: './dialog-operation.component.html',
  styleUrls: ['./dialog-operation.component.css']
})
export class DialogOperationComponent implements OnInit {

  constructor(private formbuilder : FormBuilder, private dialoref:MatDialogRef<DialogOperationComponent>, 
    @Inject(MAT_DIALOG_DATA) public editData: any, private api : ConsumerapiService
    ) { }
  clientForm !: FormGroup;

  ngOnInit(): void {
    this.clientForm = this.formbuilder.group({
      nit : ['', Validators.required],
      name : ['', Validators.required],
      lastname : ['', Validators.required],
      email : ['', Validators.required],
      phone : ['', Validators.required],
      address : ['', Validators.required],
      birthday : ['', Validators.required],
      credit_days : ['', Validators.required],
    });
    console.log(this.editData);
    if(this.editData){
      this.clientForm.controls['nit'].setValue(this.editData.nit)
      this.clientForm.controls['name'].setValue(this.editData.name)
      this.clientForm.controls['lastname'].setValue(this.editData.lastname)
      this.clientForm.controls['email'].setValue(this.editData.email)
      this.clientForm.controls['phone'].setValue(this.editData.phone)
      this.clientForm.controls['address'].setValue(this.editData.address)
      this.clientForm.controls['birthday'].setValue(this.editData.birthday)
      this.clientForm.controls['credit_days'].setValue(this.editData.credit_days)
    }
 }
 editDataPerson(){
  this.api.updatePerson(this.clientForm.value, this.editData.id).subscribe({
    next:(res)=>{
      alert("Persona Actualizada ")
      this.clientForm.reset()
      this.dialoref.close('update')
    },
    error:()=>{
      alert("Actualización erronea")
    }
  })
}
}    
  

