import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConsumerapiService } from 'src/app/consumerapi.service';
import { DialogOperationComponent } from 'src/app/dialog-operation/dialog-operation.component';
import { AddClientComponent } from '../add-client/add-client.component';

@Component({
  selector: 'app-show-client',
  templateUrl: './show-client.component.html',
  styleUrls: ['./show-client.component.css']
})
export class ShowClientComponent implements OnInit {
  title = "Lista de clientes"

  @ViewChild(MatPaginator) paginator!:MatPaginator;
  @ViewChild(MatSort) sort!:MatSort;

  constructor(private _consumerservice:ConsumerapiService, private dialog: MatDialog) { }
  persons:any=[];
  displayedColumns: string[] = ['name', 'lastname', 'email', 'phone', 'type_person', 'action'];
  dataSource: MatTableDataSource<any>=new MatTableDataSource<any>();

  ngOnInit(): void {
    this.getAllPerson();
  }
  getAllPerson(){
    this._consumerservice.getListPerson().subscribe(
      (PersonData)=>{
        this.persons=PersonData
        this.dataSource = new MatTableDataSource<any>(this.persons);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    )
  }

  editPerson(element : any){
    this.dialog.open(DialogOperationComponent,{
      width: '40%',
      data:element
    }).afterClosed().subscribe(val=>{
      if(val === 'update'){
        this.getAllPerson();
      }
    });
  }

  addiPerson(){
    this.dialog.open(AddClientComponent,{
      width: '40%',
    }).afterClosed().subscribe(val=>{
      if(val === 'update'){
        this.getAllPerson();
      }
    });
  }
  deletePerson(id:number){
    this._consumerservice.deletePerson(id).subscribe({
      next:(res)=>{
        alert("Persona eliminada con eexito");
        this.getAllPerson();
      },
      error:()=>{
        alert("Acción no completada");
      }
    })

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
