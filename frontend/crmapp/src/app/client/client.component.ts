import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ConsumerapiService } from '../consumerapi.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  constructor(private _consumerservice:ConsumerapiService) { }
  persons:any=[];
  displayedColumns: string[] = ['name', 'lastname', 'email', 'phone', 'type_person', 'action'];
  dataSource: MatTableDataSource<any>=new MatTableDataSource<any>();

  ngOnInit(): void {
    this._consumerservice.getListPerson().subscribe(
      (PersonData)=>{
        this.persons=PersonData
        this.dataSource = new MatTableDataSource<any>(this.persons);
      }
    )
  }

}
