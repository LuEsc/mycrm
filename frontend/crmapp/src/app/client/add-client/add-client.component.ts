import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConsumerapiService } from 'src/app/consumerapi.service';
import { DialogOperationComponent } from 'src/app/dialog-operation/dialog-operation.component';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  clientForm !: FormGroup;
  constructor(private formbuilder : FormBuilder, private api : ConsumerapiService, 
    ) { }
  personsType:any=[];


  ngOnInit(): void {
    this.api.getListTypeP().subscribe(
      (TypeP)=>{
        this.personsType=TypeP
      }
    )
    this.clientForm = this.formbuilder.group({
      nit : ['', Validators.required],
      name : ['', Validators.required],
      lastname : ['', Validators.required],
      email : ['', Validators.required],
      phone : ['', Validators.required],
      address : ['', Validators.required],
      birthday : ['', Validators.required],
      credit_days : ['', Validators.required],
    })
  }
  addPerson(){
    //console.log(this.clientForm.value);
    if(this.clientForm.valid){
      this.api.addPerson(this.clientForm.value)
      .subscribe({
        next:(res)=>{
          alert("Persona creada y agregada")
          this.clientForm.reset()
        },
        error:()=>{
          alert("no fue creada la persona")
        }
      })
    }
  }
  
}
