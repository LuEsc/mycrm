import { TestBed } from '@angular/core/testing';

import { ConsumerapiService } from './consumerapi.service';

describe('ConsumerapiService', () => {
  let service: ConsumerapiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsumerapiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
