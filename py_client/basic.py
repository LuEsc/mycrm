# py_client es creado con la intención de poder hacer una consulta a los datos mi api backend
# en pocas palabras me permite consumir mi api creada en BACKEND

import requests

# Endponit o puerta de acceso al servidor donde está la api
endpoint = "http://localhost:8000/api/" 

get_response = requests.get(endpoint, json={'query': 'Hola'})
print(get_response.json())
#print(get_response.status_code)